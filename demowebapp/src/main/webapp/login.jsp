<%@ page import="com.db.service.DBService" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.db.entity.Deal" %>
<%@ page import="java.util.List" %>
<%@ page import="com.db.database.ApplicationScopeHelper" %><%--
    Document   : index
    Created on : 17-Jun-2016, 16:53:18
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper2" class="com.db.database.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="">
	    <meta name="author" content="">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	    <script src="dbanalyzer\js\signin.js"></script>
        <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous">
        </script>
        <link href="dbanalyzer/css/signin.css" rel="stylesheet">
        <link rel="icon" href="dbanalyzer/images/db_logo.jpg">
        <title>Deutsche Bank Case Study</title>
    </head>
    
    <body class="text-center">
        <%
        	String dbStatus = "Database connection offline.";
        	ApplicationScopeHelper globalHelper = new ApplicationScopeHelper();
            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();
            if( connectionStatus )
            {
                dbStatus = "Database connection online";
            }
        %>
	  <form id="signin-form"class="form-signin">
	  	  <img class="mb-4" src="dbanalyzer/images/db_logo.jpg" alt="" width="72" height="72">
	      <h6><%=dbStatus%></h6>
	      <h6 id="login-status">Please sign in</h6>
	      <label for="inputEmail" class="sr-only">Username</label>
	      <input name = "usr" id="inputEmail" class="form-control" placeholder="Username" required autofocus>
	      <label for="inputPassword" class="sr-only">Password</label>
	      <input name = "pwd" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
	      <div class="checkbox mb-3">
	        <label>
	          <input type="checkbox" value="remember-me"> Remember me
	        </label>
	      </div>
	      <button class="btn btn-lg btn-primary btn-block" type="button" id="signin-button" onclick="submittest()">Sign in</button>
	      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
      </form>

    </body>
</html>
