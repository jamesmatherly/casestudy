var rootURL = "rws/services"
var tableData = null;

function submittest() {
	fds = $("#signin-form").serialize();
	$("#signin-form").trigger("reset");
	var request = $.ajax({
		method: 'POST',
		url: rootURL + '/login',
		dataType: "json", // data type of response
		data: fds,
                
		success: function (result) {
			$("#login-status").text("Sign in successful. Redirecting...")
			setTimeout(function(){
				window.location.replace("tables.html");
			}, 2000);
		}
	});
	request.fail(function( jqXHR, textStatus, errorThrown )
    {
        $("#login-status").text("Invalid login credentials")
    });
}

$(function() {
	$('.form-control').keypress(function (e) {
		if (e.which == 13) {
			submittest();
			this.blur();
		    return false;
		}
	});
});