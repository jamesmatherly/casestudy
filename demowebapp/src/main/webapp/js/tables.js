var url = "rws/services/"
var tableData = null;
var tableName = null;
$(document).ready(function() {
    $('ul.sidebar a').click(function(){
        event.preventDefault();
        tableName = $(this)[0].id;
        $.getJSON(url, url + tableName, function(result){
            $.each(result, function(i, item){
                $("#dataTable").append(
                    $('<td>').text(item.deal),
                    $('<td>').text(item.company),
                    $('<td>').text(item.UID)
                );
            });
        });
    });
});                                                      