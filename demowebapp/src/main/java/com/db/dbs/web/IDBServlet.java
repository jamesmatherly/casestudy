/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.dbs.web;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Selvyn
 */
public interface IDBServlet
{

    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginWithInfoFromForm(@FormParam("usr") String usr,
                                          @FormParam("pwd") String pwd);

    @GET
    @Path("/status")
    @Produces(MediaType.TEXT_HTML)
    public Response getStatus();

    @GET
    @Path("/getAllDeals")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllDeals( );

    @GET
    @Path("/getAllInstruments")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllInstruments( );


    @GET
    @Path("/getAverageBuyAndSell")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAverageBuyAndSell( );


    @GET
    @Path("/getEndingPosition")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getEndingPosition( );


    @GET
    @Path("/getRealizedProfit")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getRealizedProfit( );



    @GET
    @Path("/getEffectiveProfit")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getEffectiveProfit( );

}
