/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.dbs.web;

import com.db.dbs.utils.SimpleJsonMessage;
import com.db.entity.*;
import com.db.service.DBService;
import com.db.util.JsonResponse;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author Selvyn
 */
@Path("/services")
public class DBServlet implements IDBServlet {
    final static org.apache.log4j.Logger LOGGER = Logger.getLogger(DBServlet.class);
    //final   UserController userController = new UserController();

    @Override
    @POST
    @Path("/login")
    public Response loginWithInfoFromForm( @FormParam("usr")String usr,
                                        @FormParam("pwd")String pwd )
    {
    	System.out.println("<loginWIthInfoFromForm> usr: " + usr + " pwd: " + pwd);
        try {
		    if(DBService.validateUser(usr, pwd)) {
		    	System.out.println("<loginWithInfoFromForm> Good Job");
		        return Response.ok(new SimpleJsonMessage("Good Job"), MediaType.APPLICATION_JSON_TYPE).build();
		    }
        }
        catch (SQLException e) {
        	e.printStackTrace();
        }
        return Response.status(400).entity(new SimpleJsonMessage("User could not be found")).build();
    }

    @GET
    @Path("/status")
    public Response getStatus() {
        String status = null;
        try {
            status = (DBService.getAllDeals().toString() == null) ? "Disconnected" : "Connected";
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String result = "<html> " + "<title>" + "DBStatus 2: Electric Boogaloo" + "</title>"
                + "<body><h1>" + status + "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }

    @GET
    @Path("/getAllDeals")
    public Response getAllDeals() {
        List <Deal> dealList = null;
        Response response = null;
        try {
            dealList = DBService.getAllDeals();
            response = JsonResponse.toJson(dealList);
        } catch (SQLException | IOException e) {
            LOGGER.error("failed getAllDeals!");
            e.printStackTrace();
        }
        return response;
    }

    @GET
    @Path("/getAllInstruments")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllInstruments( ) {
        List <Instrument> resultList = null;
        Response response = null;
        try {
            resultList = DBService.getAllInstruments();
            response = JsonResponse.toJson(resultList);
        } catch (SQLException | IOException e) {
            LOGGER.error("failed getAllInstruments!");
            e.printStackTrace();
        }
        return response;
    }

    @GET
    @Path("/getAverageBuyAndSell")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAverageBuyAndSell( ) {
        List <AverageBuyAndSell> resultList = null;
        Response response = null;
        try {
            resultList = DBService.getAverageBuyAndSell();
            response = JsonResponse.toJson(resultList);
        } catch (SQLException | IOException e) {
            LOGGER.error("failed getAverageBuyAndSell!");
            e.printStackTrace();
        }
        return response;
    }

    @GET
    @Path("/getEndingPosition")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getEndingPosition( ) {
        List <EndingPosition> resultList = null;
        Response response = null;
        try {
            resultList = DBService.getEndingPosition();
            response = JsonResponse.toJson(resultList);
        } catch (SQLException | IOException e) {
            LOGGER.error("failed getEndingPosition!");
            e.printStackTrace();
        }
        return response;
    }

    @GET
    @Path("/getRealizedProfit")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getRealizedProfit( ) {
        List <RealizedProfit> resultList = null;
        Response response = null;
        try {
            resultList = DBService.getRealizedProfit();
            response = JsonResponse.toJson(resultList);
        } catch (SQLException | IOException e) {
            LOGGER.error("failed getRealizedProfit!");
            e.printStackTrace();
        }
        return response;
    }

    @GET
    @Path("/getEffectiveProfit")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getEffectiveProfit( ) {
        List <EffectiveProfit> resultList = null;
        Response response = null;
        try {
            resultList = DBService.getEffectiveProfit();
            response = JsonResponse.toJson(resultList);
        } catch (SQLException | IOException e) {
            LOGGER.error("failed getEffectiveProfit!");
            e.printStackTrace();
        }
        return response;
    }
}
