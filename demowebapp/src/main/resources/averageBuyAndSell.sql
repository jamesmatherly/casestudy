SELECT
    instrument_name,
    AVG(CASE
        WHEN deal_type = 'B' THEN deal_amount
    END) AS average_buy_price,
    AVG(CASE
        WHEN deal_type = 'S' THEN deal_amount
    END) AS average_sell_price
FROM
    deal
        JOIN
    counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id
        JOIN
    instrument ON instrument.instrument_id = deal.deal_instrument_id
GROUP BY deal_instrument_id;