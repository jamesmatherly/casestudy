package com.db.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

public class UsersDaoTest {
	public ApplicationScopeHelper ash = new ApplicationScopeHelper();
	public UsersDao dao = new UsersDao();

	@Before
	public void setUp() {
		ash.bootstrapDBConnection();
	}
	
	@Test
	public void testUsersDaoCorrect() throws SQLException {
		assertTrue(dao.checkUser("alison", "gradprog2016@07"));
		assertTrue(dao.checkUser("debs", "gradprog2016@02"));
		assertTrue(dao.checkUser("estelle", "gradprog2016@05"));
		assertTrue(dao.checkUser("john", "gradprog2016@03"));
		assertTrue(dao.checkUser("pauline", "gradprog2016@04"));
		assertTrue(dao.checkUser("samuel", "gradprog2016@06"));
		assertTrue(dao.checkUser("selvyn", "gradprog2016"));
	}
	
	public void testUsersDaoFalse() throws SQLException{
        assertFalse(dao.checkUser("Selvon", "gradprog2016"));
        assertFalse(dao.checkUser("alison", "gradprog2016@05"));
        assertFalse(dao.checkUser("john", "gradprog2016@09"));
        assertFalse(dao.checkUser("pauline", "gradprog2016"));
        assertFalse(dao.checkUser("Selvyn", "gradprog2016"));
        assertFalse(dao.checkUser("Selvon", "gradprog2016"));
    }

}
