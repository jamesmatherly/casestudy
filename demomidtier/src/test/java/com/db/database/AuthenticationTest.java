package com.db.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class AuthenticationTest {
	private ApplicationScopeHelper ash = new ApplicationScopeHelper();
    private Authentication testDAO;
    
    @Before
    public void setUp() {
    	ash.bootstrapDBConnection();
    }

    @Test
    public void testDoesUserExistTrue(){
        testDAO = new Authentication("selvyn", "gradprog2016");
        assertTrue(testDAO.getIsUser());
    }

    @Test
    public void testDoesUserExistFalse(){
        testDAO = new Authentication("Selvon", "gradprog2016");
        assertFalse(testDAO.getIsUser());
    }

    @Test
    public void testAuthenticateTrue(){
        testDAO = new Authentication("selvyn", "gradprog2016");
        assertTrue(testDAO.getIsAuthenticated());
    }

    @Test
    public void testAuthenticateFalse(){
        testDAO = new Authentication("Selvyn", "gradprog2018");
        assertFalse(testDAO.getIsAuthenticated());
    }

    @Test
    public void testGetUserDetailsTrue(){
        testDAO = new Authentication("selvyn", "gradprog2016");
        assertEquals("gradprog2016", testDAO.getRetrievedPassword());
    }
    @Test
    public void testGetUserDetailsFalse(){
        testDAO = new Authentication("Selvyn", "gradprog2018");
        assertNotEquals("gradprog2016", testDAO.getRetrievedPassword());
    }

}
