package com.db.entity;

public class EndingPosition {
	
    private String counterpartyName;
    private String instrumentName;
    private long endingPosition;

    public EndingPosition(String counterpartyName, String instrumentName, long endingPosition) {
        this.counterpartyName = counterpartyName;
        this.instrumentName = instrumentName;
        this.endingPosition = endingPosition;
    }

    public String getCounterpartyName() {
        return this.counterpartyName;
    }

    public String getInstrumentName() {
        return this.instrumentName;
    }

    public long getEndingPosition() {
        return this.endingPosition;
    }
}

