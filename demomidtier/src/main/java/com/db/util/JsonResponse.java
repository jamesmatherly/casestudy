package com.db.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.IOException;

public class JsonResponse {

    public static Response toJson(Object obj) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return Response.status(200)
                .type("application/json")
                .entity(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj))
                .build();
//        response.setContentType("application/json");
//        response.setStatus(HttpServletResponse.SC_OK);
//        response.getWriter().print(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj));
//        response.getWriter().flush();
    }

    public static void unAuthResponse(final HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().print("unauthorized");
    }

    public static void dbConnFailed(final HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        response.getWriter().print("databaseConnectionFailed");
    }

}
