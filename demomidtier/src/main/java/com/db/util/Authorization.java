package com.db.util;

import javax.servlet.http.HttpServletRequest;

public class Authorization {
    public static boolean checkAuth(HttpServletRequest request) {
        Object auth = request.getSession().getAttribute("authorized");
        return auth != null && auth.equals("true");
    }
}
