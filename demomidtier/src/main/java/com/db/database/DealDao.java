package com.db.database;

import com.db.entity.Deal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class DealDao {
    private static final String QUERY_GET_ALL_DEALS =
            "SELECT deal_id, deal_time, deal_type, deal_amount, deal_quantity, deal_instrument_id, counterparty_name,instrument_name\n" +
                    "FROM deal\n" +
                    "JOIN counterparty c ON deal.deal_counterparty_id = c.counterparty_id\n" +
                    "JOIN instrument i ON deal.deal_instrument_id = i.instrument_id";
    private static DBConnector conn = DBConnector.getConnector();

    public List<Deal> getAllDeals() throws SQLException {
        return getDealList(QUERY_GET_ALL_DEALS);
    }

    public List<Deal> getDealsPage(int pageNumber, int pageSize) throws SQLException {
        if (pageNumber <= 0) return new LinkedList<>();
        String query = String.format(QUERY_GET_ALL_DEALS + " LIMIT %d, %d",
                pageSize * (pageNumber - 1), pageSize);
        return getDealList(query);
    }

    private List<Deal> getDealList(String query) throws SQLException {
        ResultSet resultSet = conn.query(query);
        List<Deal> dealList = new LinkedList<>();
        while (resultSet.next()) {
            dealList.add(new Deal(
                    resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getLong(5),
                    resultSet.getLong(6),
                    resultSet.getString(7),
                    resultSet.getString(8)));
        }
        return dealList;
    }

    public Deal getDealById(Long deal_id) throws SQLException {
        String query = QUERY_GET_ALL_DEALS + " WHERE deal_id = " + deal_id;
        ResultSet result = conn.query(query);
        if (result.next()) {
            return new Deal(
                    result.getLong(1),
                    result.getString(2),
                    result.getString(3),
                    result.getString(4),
                    result.getLong(5),
                    result.getLong(6),
                    result.getString(7),
                    result.getString(8));
        } else {
            return null;
        }
    }
}