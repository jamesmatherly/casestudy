package com.db.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersDao {
    private static DBConnector conn = DBConnector.getConnector();
    private static final String ACCESS_QUERY = "SELECT EXISTS(SELECT * FROM users " +
            "WHERE user_id = ? AND user_pwd = ?)";

    public boolean checkUser(String login, String password) throws SQLException {
        PreparedStatement stmt = conn.getConnection().prepareStatement(ACCESS_QUERY);
        stmt.setString(1, login);
        stmt.setString(2, password);
        ResultSet result = stmt.executeQuery();
        result.next();
        System.out.println("<checkUser> result: " + result.getBoolean(1));
        return result.getBoolean(1);
    }

}
