/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;

import org.apache.log4j.Logger;

/**
 * @author Selvyn
 */
public class DBConnector {
    final static org.apache.log4j.Logger LOGGER = Logger.getLogger(DBConnector.class);

    static private DBConnector itsSelf = null;

    private Connection itsConnection;
    private String dbDriver = "";   //  "com.mysql.jdbc.Driver";
    private String dbPath = "";    //  "jdbc:mysql://52.209.91.145/";
    private String dbName = "";    //  "db_grad_cs_1916";
    private String dbUser = "";    //  "selvyn";
    private String dbPwd = "";     //  "dbGradProg2016";
    
    private final String DBOPTIONS = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private DBConnector() {
    }

    static public DBConnector getConnector() {
        if (itsSelf == null) {
            itsSelf = new DBConnector();
        }
        return itsSelf;
    }

    public Connection getConnection() {
        return itsConnection;
    }

    public boolean connect(Properties properties) {
        boolean result = false;
        try {
            dbDriver = properties.getProperty("dbDriver");
            dbPath = properties.getProperty("dbPath");
            dbName = properties.getProperty("dbName");
            dbUser = properties.getProperty("dbUser");
            dbPwd = properties.getProperty("dbPwd");
            
            System.out.println(dbPath + dbName + DBOPTIONS);

            Class.forName(dbDriver);

            itsConnection = DriverManager.getConnection(dbPath + dbName + DBOPTIONS,
                    dbUser,
                    dbPwd);

            /*
            MainUnit.log( itsConnection.getCatalog() );

            DatabaseMetaData metaInfo = itsConnection.getMetaData();

            // The following call returns a result set with following columns
            // TABLE_SCHEM String => schema name
            // TABLE_CATALOG String => catalog name (may be null)
            ResultSet rs = metaInfo.getSchemas();

            CatalogInfoIterator cursor = new CatalogInfoIterator( rs );

            while( cursor.next() )
            {
                System.out.println( cursor.getTable_Schema() + "/" + cursor.getTable_Catalog() );
            }

            rs.close();
            */
            LOGGER.info("Successfully connected to " + dbName);

            result = true;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        LOGGER.info("On Exit -> DBConnector.connect()");

        return result;
    }

    public ResultSet query(String query) {
        /* Get connection from pool and execute query */
        LOGGER.info(query);
        System.out.println(query);
        CachedRowSet crs = null;
        Connection conn = itsSelf.getConnection();
        try (Statement statement = conn.createStatement();
             ResultSet results = statement.executeQuery(query)) {
            crs = RowSetProvider.newFactory().createCachedRowSet();
            crs.populate(results); // Store results into CRS so we can close ResultSet
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return crs;
    }
}
