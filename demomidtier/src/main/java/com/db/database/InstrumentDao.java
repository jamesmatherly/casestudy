package com.db.database;

import com.db.entity.Instrument;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class InstrumentDao {

    private static final String QUERY_GET_ALL_INSTRUMENT = "SELECT instrument_id, instrument_name FROM instrument";
    private static DBConnector conn = DBConnector.getConnector();


    public List<Instrument> getAllInstruments() throws SQLException {
        return getInstrumentList(QUERY_GET_ALL_INSTRUMENT);
    }

    private List<Instrument> getInstrumentList(String query) throws SQLException {
        ResultSet resultSet = conn.query(query);
        List<Instrument> instrumentList = new LinkedList<>();
        while (resultSet.next()) {
            instrumentList.add(new Instrument(resultSet.getLong(1), resultSet.getString(2)));
        }
        return instrumentList;
    }

    public Instrument getInstrumentById(Long deal_id) throws SQLException {
        String query = QUERY_GET_ALL_INSTRUMENT + " WHERE instrument_id = " + deal_id;
        ResultSet result = conn.query(query);
        if (result.next()) {
            return new Instrument(result.getLong(1), result.getString(2));
        } else {
            return null;
        }
    }
}
