package com.db.service;

import com.db.database.DBConnector;
import com.db.database.DealDao;
import com.db.database.InstrumentDao;
import com.db.database.UsersDao;
import com.db.entity.*;
import com.db.util.ResourceReader;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class DBService {

    private static DBConnector conn = DBConnector.getConnector();
    public static boolean validateUser(String login, String password) throws SQLException {
        return new UsersDao().checkUser(login, password);
    }

    public static Deal getDealById(Long deal_id) throws SQLException {
        return new DealDao().getDealById(deal_id);
    }

    public static List<Deal> getAllDeals() throws SQLException {
        return new DealDao().getAllDeals();
    }

    public static List<Instrument> getAllInstruments() throws SQLException {
        return new InstrumentDao().getAllInstruments();
    }

    public static Instrument getInstrumentByID(long instrument_id) throws SQLException {
        return new InstrumentDao().getInstrumentById(instrument_id);
    }

    public static List<Deal> getDealsPage(int pageNumber, int pageSize) throws SQLException {
        return new DealDao().getDealsPage(pageNumber, pageSize);
    }

    public static List<Deal> getDealsPage(int pageNumber) throws SQLException {
        return getDealsPage(pageNumber, 1000);
    }


    public static List<AverageBuyAndSell> getAverageBuyAndSell()  throws SQLException {
        List<AverageBuyAndSell> result = new LinkedList<>();
        try {
            String query = ResourceReader.getQuery("averageBuyAndSell.sql");
            //dao call
            ResultSet resultSet = conn.query(query);
            while (resultSet.next()) {
                result.add(new AverageBuyAndSell(
                        resultSet.getString(1),
                        resultSet.getDouble(2),
                        resultSet.getDouble(3)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<EndingPosition> getEndingPosition()  throws SQLException {
        List<EndingPosition> result = new LinkedList<>();
        try {
            String query = ResourceReader.getQuery("endingPosition.sql");
            //dao call
            ResultSet resultSet = conn.query(query);
            while (resultSet.next()) {
                result.add(new EndingPosition(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getLong(3)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<RealizedProfit> getRealizedProfit()  throws SQLException {
        List<RealizedProfit> result =  new LinkedList<>();
        try {
            String query = ResourceReader.getQuery("realizedProfitOrLoss.sql");
            //dao call
            ResultSet resultSet = conn.query(query);
            while (resultSet.next()) {
                result.add(new RealizedProfit(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getDouble(3)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<EffectiveProfit> getEffectiveProfit()  throws SQLException {
        List<EffectiveProfit> result =  new LinkedList<>();
        try {
            String query = ResourceReader.getQuery("effectiveProfitOrLoss.sql");
            //dao call
            ResultSet resultSet = conn.query(query);
            while (resultSet.next()) {
                result.add(new EffectiveProfit(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getDouble(3)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

//    public static boolean checkConnection() {
//        return DBConnector.getInstance().isConnected();
//    }
}