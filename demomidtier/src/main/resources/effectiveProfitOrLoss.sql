use `db_grad_normalized`;

SELECT
    counterparty_name,
    instrument_name,
    CASE
        WHEN
            SUM(CASE
                WHEN deal_type = 'B' THEN deal_quantity
                ELSE - 1 * deal_quantity
            END) > 0
        THEN
            SUM(CASE
                WHEN deal_type = 'B' THEN deal_quantity
                ELSE - 1 * deal_quantity
            END) * (SELECT
                    AVG(deal_amount / deal_quantity)
                FROM
                    deal
                WHERE
                    deal_type = 'S'
                        AND d.deal_instrument_id = deal_instrument_id)
        ELSE SUM(CASE
            WHEN deal_type = 'B' THEN deal_quantity
            ELSE - 1 * deal_quantity
        END) * (SELECT
                AVG(deal_amount / deal_quantity)
            FROM
                deal
            WHERE
                deal_type = 'B'
                    AND d.deal_instrument_id = deal_instrument_id)
    END AS effective_profit
FROM
    deal d
    JOIN counterparty ON counterparty.counterparty_id = d.deal_counterparty_id
    JOIN instrument ON instrument.instrument_id = d.deal_instrument_id
GROUP BY deal_counterparty_id , deal_instrument_id;
