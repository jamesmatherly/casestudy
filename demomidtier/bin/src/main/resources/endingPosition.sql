SELECT
    t.counterpartyName,
    t.instrumentName,
    buy - sell AS endingPosition
FROM
    (SELECT
            counterparty.counterpartyName,
            instrument.instrumentName,
            SUM(CASE
                WHEN deal_type = 'B' THEN deal_quantity
            END) AS buy,
            SUM(CASE
                WHEN deal_type = 'S' THEN deal_quantity
            END) AS sell
    FROM
        deal
    JOIN counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id
    JOIN instrument ON instrument.instrumentId = deal.deal_instrument_id
    GROUP BY deal_counterparty_id , deal_instrument_id) AS t;
