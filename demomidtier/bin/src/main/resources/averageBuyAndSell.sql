SELECT
    instrumentName,
    AVG(CASE
        WHEN deal_type = 'B' THEN deal_price
    END) AS average_buy_price,
    AVG(CASE
        WHEN deal_type = 'S' THEN deal_price
    END) AS average_sell_price
FROM
    deal
        JOIN
    counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id
        JOIN
    instrument ON instrument.instrumentId = deal.deal_instrument_id
GROUP BY deal_instrument_id;